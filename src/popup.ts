import { Calc } from '../src/services/calc';


class Popup {
    constructor() {
        let queryOptions = { active: true, currentWindow: true };
        chrome.tabs.query(queryOptions, tabs => {
            chrome.scripting.executeScript({
                target: { tabId: tabs[0].id as number },
                files: ['content.js']
            });
        });
        console.log('activateContentScripts');
        const button = document.getElementById('start-button') as HTMLElement;
        button.onclick = this.startButtonClick;
    }

    startButtonClick(): void {
        const calc = new Calc();
        const result = calc.calculate(2, 5) as number;
        console.log(result);
        console.log('start button clicked');
    }
}

const popup = new Popup();