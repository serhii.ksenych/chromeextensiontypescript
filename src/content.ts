import { Calc } from './services/calc';

export class Content {
    constructor() {
        console.log('content.ts constructor');
        const calc = new Calc();
        const result = calc.calculate(34, 3);
        console.log(result);
    }
}

const content = new Content();