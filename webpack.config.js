const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");

module.exports = {
    mode: 'production',
    devtool: 'cheap-module-source-map',
    entry: {
        popup: ['./src/popup.ts'],
        content: './src/content.ts',
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    plugins: [new HtmlWebpackPlugin({
        template: './src/popup.html'
    })],
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.ts$/,
            loader: 'ts-loader',
            exclude: /node_modules/
        }]
    }
}